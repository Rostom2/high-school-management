DROP DATABASE IF EXISTS I_SCHOOL;
CREATE DATABASE  I_SCHOOL;

USE I_SCHOOL;

DROP TABLE IF EXISTS professeur;
CREATE TABLE  professeur
(
	id INT 	AUTO_INCREMENT PRIMARY KEY NOT NULL, 
	nom VARCHAR (100)NOT NULL,
	prenom VARCHAR (100)NOT NULL,
	Register DATETIME DEFAULT CURRENT_TIMESTAMP
);

DROP TABLE IF EXISTS classe;
CREATE TABLE  classe
(
	classe_id INT AUTO_INCREMENT PRIMARY KEY NOT NULL ,
	nom VARCHAR (200) NOT NULL,
	create_at DATETIME NOT NULL
);


DROP TABLE IF EXISTS eleve;
CREATE TABLE  eleve
(
	eleve_id INT 	AUTO_INCREMENT PRIMARY KEY NOT NULL, 
	nom VARCHAR (100)NOT NULL,
	prenom VARCHAR (100)NOT NULL,
	fk_classe_id INT,
	FOREIGN KEY (fk_classe_id) REFERENCES classe(classe_id)
);

DROP TABLE IF EXISTS matiere;
CREATE TABLE  matiere
(
	matiere_id INT AUTO_INCREMENT PRIMARY KEY NOT NULL ,
	nom VARCHAR (200) NOT NULL
);


DROP TABLE IF EXISTS classe_matiere;
CREATE TABLE  classe_matiere
(
	id INT 	AUTO_INCREMENT PRIMARY KEY NOT NULL, 
	fk_matiere_id INT ,
	FOREIGN KEY (fk_matiere_id) REFERENCES matiere(matiere_id),
	fk_classe_id INT,	
	FOREIGN KEY (fk_classe_id) REFERENCES classe(classe_id)
);


DROP TABLE IF EXISTS note;
CREATE TABLE  note
(
	id INT 	AUTO_INCREMENT PRIMARY KEY NOT NULL,
	valeurnote INT NOT NULL,  
	fk_matiere_id INT,
	FOREIGN KEY (fk_matiere_id) REFERENCES matiere(matiere_id),
	fk_eleve_id INT,
	FOREIGN KEY (fk_eleve_id) REFERENCES eleve(eleve_id),
	create_at DATETIME  NOT NULL
);



INSERT INTO professeur (nom, prenom)
 VALUES
 ('CHouki', 'youb'),
 ('nichoune', 'nichoune'),
 ('Marielle', 'marielle'),
 ('shodin', 'yoki');

INSERT INTO eleve(nom, prenom)
 VALUES
 ('e1','youn'),
 ('e2', 'ien'),
 ('e3','beleo'),
 ('e4','mokchou'),
 ('e5','tu'),
 ('e6','ien'),
 ('e7','hello'),
 ('e8','super');

 INSERT INTO classe (nom)
 VALUES
 ('2nd1'),
 ('2nd2'),
 ('2nd3'),
 ('1er1'),
 ('1er2'),
 ('1er3'),
 ('Tml1'),
 ('Tml2'),
 ('Tml3');

 INSERT INTO matiere (nom)
 VALUES
 ('Math'),
 ('histoire'),
 ('geographie'),
 ('physique'),
 ('chimie'),
 ('phylosophie'),
 ('langues'),
 ('LV2'),
 ('biologie');

 INSERT INTO note (valeurnote,fk_matiere_id,fk_eleve_id)
 VALUES
 (20,2,4),
 (20,5,2),
 (20,6,3),
 (20,1,1);