package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;

import Model.Fen_Ajout_Eleve;
import Model.Fen_Ajout_Professeur;
import Model.Fen_Resultat;
import Model.Fen_ajout_matiere;
import Model.fen_charge;
import controller.ges_cours;
import controller.ges_eleve;
import controller.ges_prof;
import controller.ges_resultat;
import controller.listEleve;
import controller.listcharge;
import controller.listcours;
import controller.listprofesseurs;
import controller.listresults;

public class Main extends JFrame implements ActionListener {

	private JMenuItem qt, appr, lisE, crep, Act_P, lisAP, lisCEP, lisEl, lisCrs, lisProf, lisAct, luse;
	private JMenuBar menu;
	private JMenu Fich, aff, app, admin, hlp;
	private JMenuItem nv, st, cont;
	private JPanel img, img2, dpimg;
	private JToolBar menubar;
	private JButton req;

	private JButton b = new JButton("   Liste des el�ves  ");
	private JButton b1 = new JButton("  Liste des matieres    ");
	private JButton b2 = new JButton("  Liste des professeurs");

	private JButton b3 = new JButton("  Les resultats  ");

	private JButton b4 = new JButton("Liste des cours /profs   ");
	private JButton b5 = new JButton("  Afficher N / P eleves    ");
	private JButton b6 = new JButton("  Afficher resultat eleves   ");
	private JButton b7 = new JButton("  Afficher Liste eleves / Professeurs    ");
	private JButton b8 = new JButton("  Afficher liste des professeurs      ");

	private JButton b001 = new JButton("  Eleve     ");
	private JButton b002 = new JButton("  professeurs  ");
	private JButton b003 = new JButton("    Charge              ");
	private JButton b004 = new JButton("  Cours   ");
	private JButton b005 = new JButton("   Resultat ");

	private JTabbedPane onglet = new JTabbedPane();
	private JPanel re = new JPanel();
	private JPanel re2 = new JPanel();
	private JPanel re1 = new JPanel();
	private JPanel re3 = new JPanel();

	// private JButton enr1;
	// javax.swing.JLabel img2;

	private String requete01 = "SELECT COLUMN_NAME as 'NOM de colonne', DATA_TYPE as 'TYPE', CHARACTER_MAXIMUM_LENGTH as 'TAILLE' FROM information_schema.COLUMNS WHERE table_name = 'eleves'";

	public <split, split1> Main() {
		JSplitPane split;
		JSplitPane split1;

		Container c = getContentPane();
		JLabel ne = new JLabel();
		ne.setIcon(new ImageIcon(""));

		// JLabel nea = new JLabel();
		ne.setPreferredSize(new java.awt.Dimension(800, 400));

		this.setTitle("Acceuil");
		this.setSize(900, 700);
		this.setLocation(250, 50);

		b.addActionListener(this);

		b1.addActionListener(this);

		b2.addActionListener(this);
		;

		b3.addActionListener(this);

		b4.addActionListener(this);

		JLabel txt = new JLabel("BIENVENUE");

		JMenuBar menu = new JMenuBar();
		setJMenuBar(menu);
		// menus
		JMenu Fich = new JMenu("archive");
		menu.add(Fich);

		Fich.setMnemonic('a');
		img = new JPanel();
		img.add(ne); // img.add(nea);

		// b.setBounds(10, 22,100, 20);

		JLabel fondimg = new JLabel();

		//
		fondimg.setSize(900, 800);

		// ONGLET
		// re.setSize(10,900);
		re.setBounds(5, 20, 218, 800);
		onglet.setSize(400, 830);
		onglet.setBorder(null);
		onglet.setBackground(Color.white);
		re.setLayout(new BorderLayout());

		re1.add(b, BorderLayout.WEST);
		b.setBackground(Color.white);
		b.setPreferredSize(new Dimension(200, 24));
		re1.add(b1, BorderLayout.WEST);
		b1.setBackground(Color.white);
		b1.setPreferredSize(new Dimension(200, 25));
		re1.add(b2, BorderLayout.WEST);
		b2.setBackground(Color.white);
		b2.setPreferredSize(new Dimension(200, 25));

		re1.add(b3, BorderLayout.WEST);
		b3.setBackground(Color.white);
		b3.setPreferredSize(new Dimension(200, 25));

		re1.add(b4, BorderLayout.WEST);
		b4.setBackground(Color.white);
		b4.setPreferredSize(new Dimension(200, 25));

		onglet.add("affichage", re1);
		re.add(onglet);
		re2.add(b5, BorderLayout.WEST);
		b5.setBackground(Color.white);
		b5.addActionListener(this);
		re2.add(b6, BorderLayout.WEST);
		b6.setBackground(Color.white);
		b6.addActionListener(this);
		re2.add(b7, BorderLayout.WEST);
		b7.setBackground(Color.white);
		b7.addActionListener(this);
		re2.add(b8, BorderLayout.WEST);
		b8.setBackground(Color.white);
		b8.addActionListener(this);

		re2.setBackground(Color.white);
		// gestion tables
		re3.add(b001, BorderLayout.WEST);
		b001.setBackground(Color.white);
		b001.setPreferredSize(new Dimension(150, 25));
		b001.addActionListener(this);
		re3.add(b002, BorderLayout.WEST);
		b002.setBackground(Color.white);
		b002.setPreferredSize(new Dimension(150, 25));
		b002.addActionListener(this);

		re3.add(b004, BorderLayout.WEST);
		b004.setBackground(Color.white);
		b004.setPreferredSize(new Dimension(150, 25));
		b004.addActionListener(this);
		re3.add(b005, BorderLayout.WEST);
		b005.setBackground(Color.white);
		b005.setPreferredSize(new Dimension(150, 25));
		b005.addActionListener(this);

		onglet.add("Gestion", re3);
		re3.setBackground(Color.white);

		onglet.add("requ�te", re2);

		re1.setBackground(Color.white);
		split = new javax.swing.JSplitPane(javax.swing.JSplitPane.VERTICAL_SPLIT, new javax.swing.JScrollPane(null),
				null);
		split.setDividerLocation(100);
		split.setDividerSize(2);
		split1 = new javax.swing.JSplitPane(javax.swing.JSplitPane.HORIZONTAL_SPLIT, re, split);
		split1.setDividerLocation(220);
		split1.setDividerSize(2);

		// MENU BARE

		menubar = new JToolBar();
		menubar.setBounds(5, 2, 1400, 19);
		// button requ�te
		req = new JButton("Ex�cuter la Requ�te");
		menubar.add(req);

		// sous menu(-

		JMenu nv = new JMenu("Nouveau");
		Fich.add(nv);
		nv.setMnemonic('N');

		JMenuItem enr = new JMenuItem("Nouveau El�ve");

		nv.add(enr);
		enr.setMnemonic('e');

		enr.setBounds(0, 0, 0, 20);
		JMenuItem cr = new JMenuItem("Nouveau cours");

		nv.add(cr);
		cr.setMnemonic('c');

		JMenuItem prf = new JMenuItem("Nouveau Professeurs");
		prf.setMnemonic('p');

		nv.add(prf);

		JMenuItem nvrlt = new JMenuItem("Nouveau resultats des eleves");

		nv.add(nvrlt);
		nvrlt.setMnemonic('r');

		JMenuItem crep = new JMenuItem("Nouveau cours enseigner/prof ");

		nv.add(crep);

		// affichage des onglets et requ�tes

		JPanel i = new JPanel();
		i.setLayout(null);

		qt = new JMenuItem("Quitter");
		qt.setMnemonic('q');
		Fich.add(qt);

		qt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}

		});

		JMenu aff = new JMenu("Consultation");
		menu.add(aff);
		aff.setMnemonic('c');

		JMenu admin = new JMenu("admin");
		menu.add(admin);
		admin.setMnemonic('d');

		JMenu NU = new JMenu("Nouveau");
		admin.add(NU);
		NU.setMnemonic('n');

		JMenuItem us = new JMenuItem("User");
		NU.add(us);
		us.setMnemonic('u');

		JMenu lUS = new JMenu("les Users");
		admin.add(lUS);

		JMenuItem luse = new JMenuItem("liste des Utilisateurs");
		lUS.add(luse);

		JMenu st = new JMenu("Structure de table");
		aff.add(st);

		lisEl = new JMenuItem("Liste Eleve");
		st.add(lisEl);

		lisE = new JMenuItem("Liste Resultats");
		st.add(lisE);

		lisCEP = new JMenuItem("Liste des cours enseign�/profs");
		st.add(lisCEP);

		lisCrs = new JMenuItem("Liste des Cours");
		st.add(lisCrs);

		lisProf = new JMenuItem("Liste des Professeurs");
		st.add(lisProf);

		i.add(re);
		i.add(menubar);

		add(i);

		// ajouts des fenetres et ses evenements

		lisEl.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				listEleve el = new listEleve();
				el.setVisible(true);
				el.setResizable(false);

			}

		});

		us.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				/*
				 * fen_users user=new fen_users(); user.setVisible(true);
				 * user.setResizable(true);
				 */

			}
		});

		crep.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				fen_charge ch = new fen_charge();
				ch.setVisible(true);
				ch.setResizable(true);

			}

		});
		// Fenetre Resultat
		nvrlt.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				Fen_Resultat w = new Fen_Resultat();
				w.setVisible(true);
				w.setResizable(true);

			}

		});

		luse.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				/*
				 * listusers use=new listusers(); use.setVisible(true);
				 */
			}

		});

		lisE.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				listresults el = new listresults();
				el.setVisible(true);

			}
		});

		lisCEP.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				listcharge chr = new listcharge();
				chr.setVisible(true);

			}
		});

		lisProf.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				listprofesseurs pr = new listprofesseurs();
				pr.setVisible(true);

			}
		});

		lisCrs.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				listcours crs = new listcours();
				crs.setVisible(true);

			}
		});

		prf.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				Fen_Ajout_Professeur pr = new Fen_Ajout_Professeur();
				pr.setVisible(true);
				pr.setResizable(false);

			}

		});

		cr.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				Fen_ajout_matiere f = new Fen_ajout_matiere();
				f.setVisible(true);
				f.setResizable(false);

			}

		});

		enr.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Fen_Ajout_Eleve fe = new Fen_Ajout_Eleve();
				fe.setVisible(true);
				fe.setResizable(false);

			}

		});
	}

	// affichage onglets et requ�tes

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == b) {

			listEleve el = new listEleve();
			el.setVisible(true);

		}

		if (e.getSource() == b1) {

			listcours cr = new listcours();
			cr.setVisible(true);
			cr.setResizable(true);

		}

		if (e.getSource() == b2) {

			listprofesseurs pr = new listprofesseurs();
			pr.setVisible(true);

		}

		if (e.getSource() == b3) {

			listresults re = new listresults();
			re.setVisible(true);
			re.setResizable(false);

		}

		if (e.getSource() == b4) {

			listcharge ch = new listcharge();
			ch.setVisible(true);
			ch.setResizable(false);

		}

		if (e.getSource() == b5) {
			// JOptionPane.showConfirmDialog(this, "Voulez vous affichez le nom,prenom des
			// �l�ves qui sont dans l'�quipe SEQUENCE","Question",
			// JOptionPane.YES_NO_OPTION);
			if (JOptionPane.showConfirmDialog(this,
					"Voulez vous affichez le nom,prenom des �l�ves qui sont dans la classe", null,
					JOptionPane.OK_OPTION) == JOptionPane.OK_OPTION) {
				/*
				 * ici l'affichage de la requete N/P el�ves req1 r=new req1();
				 * r.setVisible(true); r.setResizable(false);
				 */}

			else if (JOptionPane.OK_CANCEL_OPTION == JOptionPane.NO_OPTION) {

			}
		}

		if (e.getSource() == b6) {
			// JOptionPane.showConfirmDialog(this, "Voulez vous affichez le nom,prenom des
			// �l�ves qui sont dans l'�quipe SEQUENCE","Question",
			// JOptionPane.YES_NO_OPTION);
			if (JOptionPane.showConfirmDialog(this, "Voulez vous affichez les r�sultats de tous les Eleves", null,
					JOptionPane.OK_OPTION) == JOptionPane.OK_OPTION) {
				/* req2 r2=new req2(); */
				/* r2.setVisible(true); */}

			else if (JOptionPane.OK_CANCEL_OPTION == JOptionPane.NO_OPTION) {

			}
		}

		if (e.getSource() == b7) {

			if (JOptionPane.showConfirmDialog(this, "Voulez vous affichez la Liste des El�ves et professeur", null,
					JOptionPane.OK_OPTION) == JOptionPane.OK_OPTION) {
				/*
				 * req3 r3=new req3(); r3.setVisible(true); r3.setResizable(false);
				 */}

			else if (JOptionPane.OK_CANCEL_OPTION == JOptionPane.NO_OPTION) {
			}
		}

		if (e.getSource() == b8) {

			if (JOptionPane.showConfirmDialog(this, "Voulez vous affichez la Liste des Professeurs ", null,
					JOptionPane.OK_OPTION) == JOptionPane.OK_OPTION) {
				/*
				 * req5 r5=new req5(); r5.setVisible(true); r5.setResizable(false);
				 */ }

			else if (JOptionPane.OK_CANCEL_OPTION == JOptionPane.NO_OPTION) {
			}
		}
		if (e.getSource() == b001) {

			ges_eleve ge = new ges_eleve();
			ge.setVisible(true);

		}

		if (e.getSource() == b002) {

			ges_prof gp = new ges_prof();
			gp.setVisible(true);
			gp.setResizable(false);

		}

		if (e.getSource() == b004) {

			ges_cours gcrs = new ges_cours();
			gcrs.setVisible(true);
			gcrs.setResizable(false);

		}

		if (e.getSource() == b005) {

			ges_resultat grst = new ges_resultat();
			grst.setVisible(true);
			grst.setResizable(false);

		}

	}

	public static void main(String[] args) {

		Main x = new Main();
		x.setVisible(true);

		x.setResizable(false);

	}

}
