package Model;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import javax.sound.sampled.Clip;
import javax.swing.*;
import javax.swing.text.AbstractDocument.Content;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.awt.CardLayout;
import java.awt.Dimension;
class resultatRecherche extends JFrame {

    private ResultatJoseph tableauResultat = new ResultatJoseph();
    /**
     * renvoit une JFrame qui contient le JTableau des résultat de la query
     * @param query
     */
    public resultatRecherche(String query) {
        this.setTitle("Résultats");
        this.setSize(800,500);
        this.setLocationRelativeTo(null);
        this.getContentPane().add(new JScrollPane(allAfficher(query)));

        this.setVisible(true);
        FramPrincipal.getInstance().add(new JScrollPane(allAfficher(query)));
    }
    /**
     * renvoit une JTable avec tout les résultats de la query
     * @param query
     * @return
     */
    public JTable allAfficher(String query) {

        //	String query = "SELECT * FROM eleve";
        String affiche=null;
        Object[][]data = new Object[0][0];
        JTable table = new JTable();

        try {
            ResultSet result = ConnenxionSQL.connexion().executeQuery(query);
            ResultSetMetaData meta = result.getMetaData();
            Object[] colonne = new Object[meta.getColumnCount()];
            for(int i=1 ; i<=meta.getColumnCount() ; i++) {
                colonne[i-1]=meta.getColumnName(i);
            }
            result.last();
            int lignes = result.getRow();
            data = new Object[lignes][meta.getColumnCount()];
            result.beforeFirst();
            int j=1;
            while(result.next()) {
                for(int i=1 ; i<=meta.getColumnCount() ; i++) {
                    data[j-1][i-1] = result.getObject(i);
                }
                j++;
            }
            table = new JTable(data,colonne);
        }
        catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return table;
    }

}
class ResultatJoseph {
    /**
     * renvoit une JTable avec tout les résultats de la query
     * @param query
     * @return
     */
    public JTable allAfficher(String query) {

        //	String query = "SELECT * FROM eleve";
        String affiche=null;
        Object[][]data = new Object[0][0];
        JTable table = new JTable();

        try {
            ResultSet result = ConnenxionSQL.connexion().executeQuery(query);
            ResultSetMetaData meta = result.getMetaData();
            Object[] colonne = new Object[meta.getColumnCount()];
            for(int i=1 ; i<=meta.getColumnCount() ; i++) {
                colonne[i-1]=meta.getColumnName(i);
            }
            result.last();
            int lignes = result.getRow();
            data = new Object[lignes][meta.getColumnCount()];
            result.beforeFirst();
            int j=1;
            while(result.next()) {
                for(int i=1 ; i<=meta.getColumnCount() ; i++) {
                    data[j-1][i-1] = result.getObject(i);
                }
                j++;
            }
            table = new JTable(data,colonne);
        }
        catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return table;
    }
}

 class FramPrincipal {
    private static  JPanel content = null;
    private static FramPrincipal framPrincipal=null;
    public FramPrincipal() {
        // TODO Auto-generated constructor stub


        content = new JPanel();


        JFrame frame = new JFrame("TD HighSchool");
        CardLayout cardLayout = new CardLayout();

        frame = new JFrame();
        frame.setSize(new Dimension(1100,680));
        frame.setTitle("TD HighSchool");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);


        content.setLayout(cardLayout);


        frame.setContentPane(content);
        frame.setVisible(true);
    }
    public static JPanel getContent() {
        return content;
    }
    /**
     * retourne le content
     * @return
     */
    public static JPanel getInstance() {
        if(framPrincipal==null) {
            framPrincipal=new FramPrincipal();
        }
        return getContent();
    }
}