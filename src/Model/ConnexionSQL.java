package Model;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

class ConnenxionSQL{
    Statement state=null;
    private static ConnenxionSQL connexion=null;

    private ConnenxionSQL() {
        // TODO Auto-generated constructor stub
        state=dbConnection();
    }
    /**
     * retourne state=HighSchoolJoseph.dbConnection();
     * @return
     */
    public Statement getState() {
        return state;
    }
    /**
     * retourne une instance de la class ConnexionSQL
     * @return
     */
    public static ConnenxionSQL getInstance() {
        if (connexion==null) {
            connexion=new ConnenxionSQL();
        }
        return connexion;
    }
    /**
     * retourne une instanciation de statement
     * qui fait appel à dbConnexion
     * @return
     */
    public static Statement connexion() {

        return getInstance().getState();

    }
    /**
     * Renvoit un Statement com.mysql.jdbc.Driver
     * via l'URL jdbc:mysql://localhost/Database.sql
     * @return
     */
    public static Statement dbConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String url = "jdbc:mysql://localhost/I_SCHOOL";
        String user = "root";
        String password = "";

        Statement state = null;
        try {
            Connection con = DriverManager.getConnection(url, user, password);
            state = con.createStatement();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return state;
    }

}


  //  Ajouter un commentaireRéduire 