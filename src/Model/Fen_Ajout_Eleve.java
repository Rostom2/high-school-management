package Model;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Fen_Ajout_Eleve extends JFrame implements ActionListener {

	JTextField txtnom, txtpren, txtdat, txtnum;
	JButton bten, supp, modif, rech, fer, ann;
	Statement st;
	private javax.swing.JFormattedTextField daten = new javax.swing.JFormattedTextField(DateFormat.getDateInstance());

	// connection k = new connection();

	public Fen_Ajout_Eleve() {
		this.setTitle("Enregistrement");
		this.setSize(400, 350);
		this.setLocation(500, 200);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JPanel p = new JPanel();
		p.setBackground(Color.white);
		p.setLayout(null);
		JLabel lbnum = new JLabel("Numero d'eleve");
		JLabel lbnom = new JLabel("Nom d'eleve");
		JLabel lbpren = new JLabel("Prenom d'eleve");
		JLabel lbdat = new JLabel("Date de Naissance");
		JLabel lbT = new JLabel("AJout D'un Nouveau Eleve");

		//txtnum = new JTextField();
		txtnom = new JTextField();
		txtpren = new JTextField();
		//txtdat = new JTextField("zzzz-xx-yy");

		bten = new JButton("Sauver");
		ann = new JButton("Terminer");

		bten.addActionListener(this);
		ann.addActionListener(this);

		// boutton
		Font fe = new Font("gramound", 0, 15);

		// fer.addActionListener(this);

		// ajouts dans le panel
		lbT.setBounds(15, 0, 340, 20);
		p.add(lbT);

		/*lbnum.setBounds(30, 40, 110, 25);
		p.add(lbnum);
		lbnum.setFont(fe);
		txtnum.setBounds(200, 40, 120, 25);
		p.add(txtnum);*/

		lbnom.setBounds(30, 70, 120, 25);
		p.add(lbnom);
		lbnom.setFont(fe);
		txtnom.setBounds(200, 70, 120, 25);
		p.add(txtnom);

		lbpren.setBounds(30, 100, 120, 25);
		p.add(lbpren);
		lbpren.setFont(fe);
		txtpren.setBounds(200, 100, 120, 25);
		p.add(txtpren);

		/*lbdat.setBounds(30, 130, 120, 25);
		p.add(lbdat);
		lbdat.setFont(fe);
		txtdat.setBounds(200, 130, 120, 25);
		p.add(txtdat);*/

		// txtrec.setBounds(180,150,110,20);p.add(txtrec);

		// ajout buttons

		bten.setBounds(50, 230, 130, 27);
		p.add(bten);
		bten.setFont(fe);
		bten.setBackground(Color.CYAN);
		ann.setBounds(200, 230, 130, 27);
		p.add(ann);
		ann.setFont(fe);
		ann.setBackground(Color.CYAN);

		// CONTENU DU PANEL

		add(p);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// enregistrement

		if (e.getSource() == bten) {
			String /*nu,*/ n, p, a1;
			//nu = txtnum.getText();
			n = txtnom.getText();
			p = txtpren.getText();
			//String d = txtdat.getText();

			//String query = "Insert into eleve (nom, prenom) values('" + nu + "','" + n + "','" + p + "','" + d + "','" + "','" + "')";
			 String query = "Insert into eleve (nom, prenom) values('" + n + "','" + p + "')";
			try {

				/*
				 * ICI ce passe la connection avec la BDD
				 * 
				 * st = k.etablirconnection().createStatement();
				 *
				 */
				ConnenxionSQL.getInstance();

				if (JOptionPane.showConfirmDialog(this, "Voulez Vous Enregistrez", "Attention",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					;

			//	if (txtnum.getText().length() != 0)
				if (txtnom.getText().length() != 0)

				{
					ConnenxionSQL.getInstance().getState().execute(query);
					//st.execute(query);
					JOptionPane.showMessageDialog(this, "Enregistrez avec succes");

					//txtnum.setText("");
					txtnom.setText("");
					txtpren.setText("");
				//	txtdat.setText("");

				} else {
					JOptionPane.showMessageDialog(null, "veuillez remplire les champs !");
				}

			} catch (SQLException e1) {

				 e1.printStackTrace();
				JOptionPane.showMessageDialog(this, "erreur d'ajout");
			}

		}

		// fermeture fenetre Enregistrement

		// JOptionPane.showConfirmDialog(this, "Etes vous sure d'avoir
		// Terminier","Incident Majeur",JOptionPane.YES_NO_OPTION);
		// dispose();

		if (e.getSource() == ann) {
			// JOptionPane.showConfirmDialog(this, "Etes vous sure d'avoir
			// Terminier","Incident Majeur",JOptionPane.YES_NO_OPTION);
			// dispose();

			if (JOptionPane.showConfirmDialog(this, "Etes vous sure d'avoir Terminier", "Incident Majeur",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				dispose();
			}
		}
	}

}