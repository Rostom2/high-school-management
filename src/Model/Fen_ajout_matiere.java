package Model;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Fen_ajout_matiere extends JFrame implements ActionListener {

	JTextField txtnom, txtnum, txtnbrh;
	JButton bten, ann;
	Statement st;

	// connection k=new connection();

	public Fen_ajout_matiere() {
		this.setTitle("Enregistrement");
		this.setSize(400, 300);

		this.setLocation(500, 200);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JPanel p = new JPanel();
		p.setBackground(Color.white);
		p.setLayout(null);
		//JLabel lbnum = new JLabel("Numero du cours");
		JLabel lbnom = new JLabel("Nom de la matiere");
		JLabel lbnbr = new JLabel("Nom Professeur");

		JLabel lbT = new JLabel("INFORMATION MATIERE");

		//txtnum = new JTextField();
		txtnom = new JTextField();
		txtnbrh = new JTextField();

		bten = new JButton("Sauver");
		ann = new JButton("Annuler");

		bten.addActionListener(this);
		ann.addActionListener(this);

		// boutton
		Font fe = new Font("Serif", 0, 15);

		// ajouts dans le panel
		lbT.setBounds(130, 5, 130, 30);
		p.add(lbT);
		lbT.setForeground(Color.black);
		lbT.setPreferredSize(new Dimension(400, 200));

		/*lbnum.setBounds(30, 40, 170, 25);
		p.add(lbnum);
		lbnum.setFont(fe);
		txtnum.setBounds(200, 40, 120, 25);
		p.add(txtnum);*/

		lbnom.setBounds(30, 70, 120, 25);
		p.add(lbnom);
		lbnom.setFont(fe);
		txtnom.setBounds(200, 70, 120, 25);
		p.add(txtnom);

		lbnbr.setBounds(30, 100, 120, 25);
		p.add(lbnbr);
		lbnbr.setFont(fe);
		txtnbrh.setBounds(200, 100, 120, 25);
		p.add(txtnbrh);

		// txtrec.setBounds(180,150,110,20);p.add(txtrec);

		// ajout buttons

		bten.setBounds(50, 180, 120, 27);
		p.add(bten);
		bten.setFont(fe);
		bten.setBackground(Color.CYAN);
		bten.setIcon(new ImageIcon("C:\\Users\\mohakalil\\Desktop\\img\\save.png"));
		ann.setBounds(200, 180, 120, 27);
		p.add(ann);
		ann.setFont(fe);
		ann.setBackground(Color.CYAN);

		// CONTENU DU PANEL

		add(p);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// enregistrement

		if (e.getSource() == bten) {
			String nu, pd, d, n, p, a1;
			//nu = txtnum.getText();
			n = txtnom.getText();
			p = txtnbrh.getText();

			//String query = "Insert into cours values('" + nu + "','" + n + "','" + p + "','" + "')";
			String query = "Insert into matiere (nom) values('" + n + "','" + p + "')";

			try {

				// st=k.etablirconnection().createStatement();
				ConnenxionSQL.getInstance();

				if (JOptionPane.showConfirmDialog(this, "Voulez Vous Enregistrez", "Attention",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					;

				if (txtnom.getText().length() != 0)

				{
					//st.executeUpdate(query);
					ConnenxionSQL.getInstance().getState().execute(query);

					JOptionPane.showMessageDialog(this, "Enregistrez avec succes");

					//txtnum.setText("");
					txtnom.setText("");
					txtnbrh.setText("");

				} else {
					JOptionPane.showMessageDialog(null, "veuillez remplire les champs !");
				}

			} catch (SQLException e1) {

				// e1.printStackTrace();
				JOptionPane.showMessageDialog(this, "errure d'ajout");
			}

		}

		// fermeture fenetre Enregistrement

		if (e.getSource() == ann) {
			// JOptionPane.showConfirmDialog(this, "Etes vous sure d'avoir
			// Terminier","Incident Majeur",JOptionPane.YES_NO_OPTION);
			// dispose();

			if (JOptionPane.showConfirmDialog(this, "Etes vous sure d'avoir Terminier", "Incident Majeur",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				dispose();
			}
		}
	}

}
