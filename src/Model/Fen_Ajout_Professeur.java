package Model;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;

public class Fen_Ajout_Professeur extends JFrame implements ActionListener {

	JTextField txtnom, txtpren, txtdat, txtnum;
	JButton bten, supp, modif, rech, fer, ann;
	Statement st;
	private javax.swing.JFormattedTextField daten = new javax.swing.JFormattedTextField(DateFormat.getDateInstance());

	// connection k = new connection();
	Statement k =ConnenxionSQL.connexion();
/////////////////
	/**
	 * modifier par Jo pour la connexion à la BdD
	 */
	public Fen_Ajout_Professeur() {
		this.setTitle("Enregistrement");
		this.setSize(600, 600);
		this.setLocation(500, 200);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JPanel p = new JPanel();
		p.setBackground(Color.white);
		p.setLayout(null);
		//	JLabel lbnum = new JLabel("Numero du professeur");
		JLabel lbnom = new JLabel("Nom du professeur");
		JLabel lbpren = new JLabel("Prenom du professeur");
		//	JLabel lbdat = new JLabel("Date de Naissance");
		JLabel lbT = new JLabel("AJout d'un nouveau professeur");

		txtnum = new JTextField();
		txtnom = new JTextField();
		txtpren = new JTextField();
		txtdat = new JTextField("zzzz-xx-yy");

		bten = new JButton("Sauver");
		ann = new JButton("Terminer");

		bten.addActionListener(this);
		ann.addActionListener(this);

		// boutton
		Font fe = new Font("gramound", 0, 15);

		// fer.addActionListener(this);

		// ajouts dans le panel
		lbT.setBounds(15, 0, 340, 20);
		p.add(lbT);
	/*
		lbnum.setBounds(40, 40, 150, 25);
		p.add(lbnum);
		lbnum.setFont(fe);
	*/
		txtnum.setBounds(250, 40, 120, 25);
		p.add(txtnum);

		lbnom.setBounds(40, 70, 150, 25);
		p.add(lbnom);
		lbnom.setFont(fe);
		txtnom.setBounds(250, 70, 120, 25);
		p.add(txtnom);

		lbpren.setBounds(40, 100, 150, 25);
		p.add(lbpren);
		lbpren.setFont(fe);
		txtpren.setBounds(250, 100, 120, 25);
		p.add(txtpren);
	/*
		lbdat.setBounds(40, 130, 150, 25);
		p.add(lbdat);
		lbdat.setFont(fe);
	*/
		txtdat.setBounds(250, 130, 120, 25);
		p.add(txtdat);

		// txtrec.setBounds(180,150,110,20);p.add(txtrec);

		// ajout buttons

		bten.setBounds(50, 230, 130, 27);
		p.add(bten);
		bten.setFont(fe);
		bten.setBackground(Color.CYAN);
		ann.setBounds(300, 230, 130, 27);
		p.add(ann);
		ann.setFont(fe);
		ann.setBackground(Color.CYAN);

		// CONTENU DU PANEL

		add(p);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// enregistrement

		if (e.getSource() == bten) {
			String nu, n, p, a1;
			nu = txtnum.getText();
			n = txtnom.getText();
			p = txtpren.getText();
			String d = txtdat.getText();

			//	String query = "Insert into professor values('" + nu + "','" + n + "','" + p + "','" + d + "','" + "','"
			//			+ "')";
			String query="INSERT INTO professeur (nom, prenom)\r\n" +
					" VALUES\r\n" +
					" ('"+n+"','"+p+"')";

			try {

				/*
				 * ICI ce passe la connection avec la BDD
				 *
				 * st = k.etablirconnection().createStatement();
				 */

				if (JOptionPane.showConfirmDialog(this, "Voulez Vous Enregistrez", "Attention",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					;

				if (txtnum.getText().length() != 0)

				{
					//	st.executeUpdate(query);
					k.execute(query);
					//	k.execute(query);
					//	k.executeQuery(query);	//essayer cette ligne ou celle du dessus

					JOptionPane.showMessageDialog(this, "Enregistrez avec succes");

					txtnum.setText(" ");
					txtnom.setText(" ");
					txtpren.setText(" ");
					txtdat.setText(" ");

				} else {
					JOptionPane.showMessageDialog(null, "veuillez remplire les champs !");
				}

			} catch (SQLException e1) {

				// e1.printStackTrace();
				JOptionPane.showMessageDialog(this, "erreur d'ajout");
			}

		}

		// fermeture fenetre Enregistrement

		// JOptionPane.showConfirmDialog(this, "Etes vous sure d'avoir
		// Terminier","Incident Majeur",JOptionPane.YES_NO_OPTION);
		// dispose();

		if (e.getSource() == ann) {
			// JOptionPane.showConfirmDialog(this, "Etes vous sure d'avoir
			// Terminier","Incident Majeur",JOptionPane.YES_NO_OPTION);
			// dispose();

			if (JOptionPane.showConfirmDialog(this, "Etes vous sure d'avoir Terminier", "Incident Majeur",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				dispose();
			}
		}
	}

}
/*package Model;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;

public class Fen_Ajout_Professeur extends JFrame implements ActionListener {

	JTextField txtnom, txtpren, txtdat, txtnum;
	JButton bten, supp, modif, rech, fer, ann;
	Statement st;
	private javax.swing.JFormattedTextField daten = new javax.swing.JFormattedTextField(DateFormat.getDateInstance());

	// connection k = new connection();

	public Fen_Ajout_Professeur() {
		this.setTitle("Enregistrement");
		this.setSize(600, 600);
		this.setLocation(500, 200);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JPanel p = new JPanel();
		p.setBackground(Color.white);
		p.setLayout(null);
		JLabel lbnum = new JLabel("Numero du professeur");
		JLabel lbnom = new JLabel("Nom du professeur");
		JLabel lbpren = new JLabel("Prenom du professeur");
		JLabel lbdat = new JLabel("Date de Naissance");
		JLabel lbT = new JLabel("AJout d'un nouveau professeur");

		txtnum = new JTextField();
		txtnom = new JTextField();
		txtpren = new JTextField();
		txtdat = new JTextField("zzzz-xx-yy");

		bten = new JButton("Sauver");
		ann = new JButton("Terminer");

		bten.addActionListener(this);
		ann.addActionListener(this);

		// boutton
		Font fe = new Font("gramound", 0, 15);

		// fer.addActionListener(this);

		// ajouts dans le panel
		lbT.setBounds(15, 0, 340, 20);
		p.add(lbT);

		lbnum.setBounds(40, 40, 150, 25);
		p.add(lbnum);
		lbnum.setFont(fe);
		txtnum.setBounds(250, 40, 120, 25);
		p.add(txtnum);

		lbnom.setBounds(40, 70, 150, 25);
		p.add(lbnom);
		lbnom.setFont(fe);
		txtnom.setBounds(250, 70, 120, 25);
		p.add(txtnom);

		lbpren.setBounds(40, 100, 150, 25);
		p.add(lbpren);
		lbpren.setFont(fe);
		txtpren.setBounds(250, 100, 120, 25);
		p.add(txtpren);

		lbdat.setBounds(40, 130, 150, 25);
		p.add(lbdat);
		lbdat.setFont(fe);
		txtdat.setBounds(250, 130, 120, 25);
		p.add(txtdat);

		// txtrec.setBounds(180,150,110,20);p.add(txtrec);

		// ajout buttons

		bten.setBounds(50, 230, 130, 27);
		p.add(bten);
		bten.setFont(fe);
		bten.setBackground(Color.CYAN);
		ann.setBounds(300, 230, 130, 27);
		p.add(ann);
		ann.setFont(fe);
		ann.setBackground(Color.CYAN);

		// CONTENU DU PANEL

		add(p);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// enregistrement

		if (e.getSource() == bten) {
			String nu, n, p, a1;
			nu = txtnum.getText();
			n = txtnom.getText();
			p = txtpren.getText();
			String d = txtdat.getText();

			String query = "Insert into professor values('" + nu + "','" + n + "','" + p + "','" + d + "','" + "','"
					+ "')";

			try {

				/*
				 * ICI ce passe la connection avec la BDD
				 *
				 * st = k.etablirconnection().createStatement();
				 */

			/*	if (JOptionPane.showConfirmDialog(this, "Voulez Vous Enregistrez", "Attention",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					;

				if (txtnum.getText().length() != 0)

				{
					st.executeUpdate(query);

					JOptionPane.showMessageDialog(this, "Enregistrez avec succes");

					txtnum.setText(" ");
					txtnom.setText(" ");
					txtpren.setText(" ");
					txtdat.setText(" ");

				} else {
					JOptionPane.showMessageDialog(null, "veuillez remplire les champs !");
				}

			} catch (SQLException e1) {

				// e1.printStackTrace();
				JOptionPane.showMessageDialog(this, "erreur d'ajout");
			}

		}

		// fermeture fenetre Enregistrement

		// JOptionPane.showConfirmDialog(this, "Etes vous sure d'avoir
		// Terminier","Incident Majeur",JOptionPane.YES_NO_OPTION);
		// dispose();

		if (e.getSource() == ann) {
			// JOptionPane.showConfirmDialog(this, "Etes vous sure d'avoir
			// Terminier","Incident Majeur",JOptionPane.YES_NO_OPTION);
			// dispose();

			if (JOptionPane.showConfirmDialog(this, "Etes vous sure d'avoir Terminier", "Incident Majeur",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				dispose();
			}
		}
	}

}*/